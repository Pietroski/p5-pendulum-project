class BOB {
    constructor() {
        this.x;
        this.y;
        this.xo = 0;
        this.yo = 0;
        this.ellipse_radius = 64;
    }

    calculus() {
        this.x = r*sin(phi);
        this.y = r*cos(phi);
    }

    display() {
        this.calculus();
        stroke(0);
        fill(255);
        line(this.xo, this.yo, this.x, this.y);
        ellipseMode(CENTER);
        ellipse(this.x, this.y, this.ellipse_radius, this.ellipse_radius);
    }
}