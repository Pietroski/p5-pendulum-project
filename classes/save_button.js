class Save_Button extends Simulation_Screen {
    constructor(_posX, _posY) {
        super(_posX, _posY);
        this.name = "Save";
        this.click = false;
    }

    first_button_click_condition() {
        return (mouseX >= (this.posX - this.a/2) && 
                mouseX <= (this.posX + this.a/2) &&
                mouseY >= (this.posY - this.b/2) && 
                mouseY <= (this.posY + this.b/2) && 
                mPressed === true)
    }

    second_button_click_condition() {
        return ((this.click === true) && (mPressed === false) &&
                (mouseX >= (this.posX - this.a/2) && 
                 mouseX <= (this.posX + this.a/2) &&
                 mouseY >= (this.posY - this.b/2) && 
                 mouseY <= (this.posY + this.b/2)) === true)
    }

    third_button_click_condition() {
        return ((this.click === true) && (mPressed === false) &&
                (mouseX < (this.posX - this.a/2) && 
                 mouseX > (this.posX + this.a/2) &&
                 mouseY < (this.posY - this.b/2) && 
                 mouseY > (this.posY + this.b/2)) === false)
    }

    button() {
        stroke(0);
        fill(255);
        rectMode(CENTER);
        rect(this.posX, this.posY, this.a, this.b, this.c);
        fill(0);
        text(this.name, this.posX, this.posY); 

        if(this.first_button_click_condition()) {
            stroke(0);
            fill(150);
            rectMode(CENTER);
            rect(this.posX, this.posY, this.a, this.b, this.c);
            stroke(0); // redundancy
            fill(255);
            rect(this.posX, this.posY, this.a-20, this.b-20, this.c);
            fill(0);
            text(this.name, this.posX, this.posY);    
            
            this.click = true;
           } else if(this.second_button_click_condition())  {

                console.log("here")
                
                background_colour_saving = true;
                red_saved = red; green_saved = green; blue_saved = blue;
                this.click = false;

            } else if(this.third_button_click_condition()) {
                this.click = false;
            }
    }
}