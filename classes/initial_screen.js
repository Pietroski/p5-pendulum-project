class Initial_Screen {

    constructor() {
        this.background_colour_set_button = new Background_Colour_Setter(windowHeight*12/100);
        this.simulation_screen = new Simulation_Screen(windowWidth/2, windowHeight*90/100);
    }

    display() {
        background(205, 205, 205);
        strokeWeight(2);

        if (start == false && background_colour_selector == false) {
            this.simulation_screen.button();
            this.background_colour_set_button.button();
        } else if (start === true && background_colour_selector === false) {
            this.simulation_screen.display();
        } else if (start === false && background_colour_selector === true) {
            this.background_colour_set_button.display();
        }
    }
}