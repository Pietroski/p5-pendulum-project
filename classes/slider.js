class Slider {

    constructor(_x, _y, _colour, _colour_type) {
        this.sx = _x; this.sy = _y;
        this.a = 150; this.b = this.a/2; this.c = this.a/8;

        this.colourp = _colour;
        this.type = _colour_type;
        this.colour_name = "";

        this.colour = function() {
            switch(this.type) {
                case 0: fill(this.colourp, 0, 0); break;
                case 1: fill(0, this.colourp, 0); break;
                case 2: fill(0, 0, this.colourp); break;
            }
        }

        this.colour();

        this.text_colour_name = function() {
            switch(this.type) {
                case 0: this.colour_name = "red"; break;
                case 1: this.colour_name = "green"; break;
                case 2: this.colour_name = "blue"; break;
                default: "";
            }
        }

        this.text_colour_name();
    }

    first_button_click_condition() {
        return (mouseX >= (this.sx - this.a/2) &&
                mouseX <= (this.sx + this.a/2) &&
                mouseY >= (this.sy - this.b/2) &&
                mouseY <= (this.sy + this.b/2) &&
                mPressed)
    }

    slide() {
        if(this.first_button_click_condition()) {
            this.sx = mouseX;

            if(mouseX >= width - this.a/2) {
                this.sx = width - this.a/2;
            } else if(mouseX <= this.a/2) {
                this.sx = this.a/2
            }

            this.trail();

            stroke(map(this.sx, this.a/2, width - this.a/2, 255, 0));
            fill(150);
            rectMode(CENTER);
            rect(this.sx, this.sy, this.a, this.b, this.c);
            this.colour();
            rect(this.sx, this.sy, this.a-15, this.b-15, this.c);
            fill(map(this.sx, this.a/2, width - this.a/2, 255, 0));
            text(this.colour_name, this.sx, this.sy);

            this.colourp = parseInt(map(this.sx, this.a/2, width - this.a/2, 0, 255));
        } else {
            this.trail();

            stroke(map(this.sx, this.a/2, width - this.a/2, 255, 0));
            this.colour();
            rectMode(CENTER);
            rect(this.sx, this.sy, this.a, this.b, this.c);
            fill(map(this.sx, this.a/2, width - this.a/2, 255, 0));
            text(this.colour_name, this.sx, this.sy);
        }
    }

    trail() {
        stroke(map(this.sx, this.a/2, width - this.a/2, 255, 0));
        fill(0);
        rectMode(CENTER);
        rect(width/2, this.sy, width - this.a, this.a/8);
    }
}