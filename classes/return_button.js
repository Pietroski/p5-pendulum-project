class Return_Button {
   
    constructor(_x, _y) {
        this.x = _x; this.y = _y;
        this.ellipse_raius = 64;
        this.a = 40; this.c = 50; this.b = 8; this.rnd = 5;
        this.name = "Return";
        this.click = false;
    }

    first_button_click_condition() {
        return (mouseX >= (50 - this.ellipse_raius/2) && 
                mouseX <= (50 + this.ellipse_raius/2) &&
                mouseY >= (this.y - this.ellipse_raius/2) &&
                mouseY <= (this.y + this.ellipse_raius/2) &&
                mPressed === true)
    }

    second_button_click_condition() {
        return ((mPressed === false && this.click === true && 
                mouseX >= (50 - this.ellipse_raius/2) &&
                mouseX <= (50 + this.ellipse_raius/2) &&
                mouseY >= (this.y - this.ellipse_raius/2) &&
                mouseY <= (this.y + this.ellipse_raius/2)) === true)
    }

    third_button_click_condition() {
        return ((mPressed === false && this.click === true && 
                mouseX < (50 - this.ellipse_raius/2) &&
                mouseX > (50 + this.ellipse_raius/2) &&
                mouseY < (this.y - this.ellipse_raius/2) &&
                mouseY > (this.y + this.ellipse_raius/2)) === false)
    }

    button() {

        stroke(0);
        fill(205, 205, 205);
        ellipseMode(CENTER);
        ellipse(this.x, this.y, this.ellipse_raius, this.ellipse_raius);
        noStroke();
        fill(0);
        rectMode(CENTER);
        rect(this.x, this.y-15, this.c, this.b, this.rnd);
        rect(this.x, this.y, this.a, this.b, this.rnd);
        rect(this.x, this.y+15, this.c, this.b, this.rnd);
        stroke(0);

        if(this.first_button_click_condition()) {

            this.a = 30; this.b = 6; this.c = 40;

            stroke(0);
            fill(150, 150, 150);
            ellipseMode(CENTER);
            ellipse(this.x, this.y, this.ellipse_raius, this.ellipse_raius);

            stroke(0);
            fill(205, 205, 205);
            ellipseMode(CENTER);
            ellipse(this.x, this.y, this.ellipse_raius-10, this.ellipse_raius-10);
            noStroke();
            fill(0);
            rectMode(CENTER);
            rect(this.x, this.y-15, this.c, this.b, this.rnd);
            rect(this.x, this.y, this.a, this.b, this.rnd);
            rect(this.x, this.y+15, this.c, this.b, this.rnd);

            this.click = true;

        } else if(this.second_button_click_condition()) {

            if(start === true) {
                start = false;
            } if(background_colour_selector === true) {
                background_colour_selector = false;
            } if(start === false && 
                 background_colour_selector === false &&
                 background_colour_saving === false) {

                    red = red_saved; green = green_saved; blue = blue_saved;
                    
                    initial_screen.background_colour_set_button.sliders_setup();

                    initial_screen.background_colour_set_button.slider_red.sx =
                        initial_screen.background_colour_set_button.rgb_initial_positioning[0];
                    initial_screen.background_colour_set_button.slider_green.sx =
                        initial_screen.background_colour_set_button.rgb_initial_positioning[1];
                    initial_screen.background_colour_set_button.slider_blue.sx =
                        initial_screen.background_colour_set_button.rgb_initial_positioning[2];

                    initial_screen.background_colour_set_button.slider_red.coulourp =
                        initial_screen.background_colour_set_button.rgb[0];
                    initial_screen.background_colour_set_button.slider_green.coulourp =
                        initial_screen.background_colour_set_button.rgb[1];
                    initial_screen.background_colour_set_button.slider_blue.coulourp =
                        initial_screen.background_colour_set_button.rgb[2];
                }

                // Resets the initial pendulum state if return button clicked
                initial_screen.simulation_screen.slider_button.sbx = 0;
                g = 0.01; phi = 0; omega = 0; alpha = 0;

                this.click = false;


        } else if(this.third_button_click_condition()) {
            this.a = 40; this.c = 50; this.b = 8;
            this.click = false;
        } 
    }
}