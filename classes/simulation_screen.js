class Simulation_Screen {

    constructor(_posX, _posY) {
        this.posX = _posX;
        this.posY = _posY;
        this.a = 300; this.b = this.a/4; this.c = this.a/8;
        this.name = "Simulation";
        this.click = false;

        this.return_button = new Return_Button(50 - x_translation, 50);
        this.slider_button = new Slider_Button();
        this.bob = new BOB();
    }

    first_button_click_condition() {
        return (mouseX >= (this.posX - this.a/2) && 
                mouseX <= (this.posX + this.a/2) &&
                mouseY >= (this.posY - this.b/2) && 
                mouseY <= (this.posY + this.b/2) && 
                mPressed === true)
    }

    second_button_click_condition() {
        return ((this.click === true) && (mPressed === false) &&
                (mouseX >= (this.posX - this.a/2) && 
                 mouseX <= (this.posX + this.a/2) &&
                 mouseY >= (this.posY - this.b/2) && 
                 mouseY <= (this.posY + this.b/2)) === true)
    }

    third_button_click_condition() {
        return ((this.click === true) && (mPressed === false) &&
                (mouseX < (this.posX - this.a/2) && 
                 mouseX > (this.posX + this.a/2) &&
                 mouseY < (this.posY - this.b/2) && 
                 mouseY > (this.posY + this.b/2)) === false)
    }

    button() {

        stroke(0);
        fill(255);
        rectMode(CENTER);
        rect(this.posX, this.posY, this.a, this.b, this.c);
        fill(0);
        text(this.name, this.posX, this.posY); 

        if(this.first_button_click_condition()) {
            stroke(0);
            fill(150);
            rectMode(CENTER);
            rect(this.posX, this.posY, this.a, this.b, this.c);
            stroke(0);
            fill(255);
            rect(this.posX, this.posY, this.a-20, this.b-20, this.c);
            fill(0);
            text(this.name, this.posX, this.posY);    
            
            this.click = true;
           } else if(this.second_button_click_condition())  {
                if(start === false) {
                    start = true;
                    this.click = false;
                } if(background_colour_selector === true) {
                    background_colour_selector = false;
                } if(start === true && 
                     background_colour_selector === false && 
                     background_colour_saving === false) {

// Condition to whether the save button is not clicked before the Simulation Initialization

                    red = red_saved; green = green_saved; blue = blue_saved;

                    initial_screen.background_colour_set_button.sliders_setup();

                    initial_screen.background_colour_set_button.slider_red.sx =
                        initial_screen.background_colour_set_button.rgb_initial_positioning[0];
                    initial_screen.background_colour_set_button.slider_green.sx =
                        initial_screen.background_colour_set_button.rgb_initial_positioning[1];
                    initial_screen.background_colour_set_button.slider_blue.sx =
                        initial_screen.background_colour_set_button.rgb_initial_positioning[2];

                    initial_screen.background_colour_set_button.slider_red.coulourp =
                        initial_screen.background_colour_set_button.rgb[0];
                    initial_screen.background_colour_set_button.slider_green.coulourp =
                        initial_screen.background_colour_set_button.rgb[1];
                    initial_screen.background_colour_set_button.slider_blue.coulourp =
                        initial_screen.background_colour_set_button.rgb[2];

                } if(background_colour_saving === true) {
                    background_colour_saving = false;
                }
            } else if(this.third_button_click_condition()) {
                this.click = false;
            }
    }

    display() {
        background(red_saved, green_saved, blue_saved);

        translate(x_translation, 0);

        this.return_button.button();

        this.slider_button.slide();
        this.bob.display();
    }
}