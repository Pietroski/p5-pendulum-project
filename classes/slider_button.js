class Slider_Button {
    constructor() {
        this.sbx = 0;
        this.sby = windowHeight*95/100;
        this.a = 150; this.b = this.a/2; this.c = this.a/4;
        this.click = false;
        this.begin = false;
    }

    first_button_click_condition() {
        return (mouseX >= (this.sbx - this.a/2) + x_translation &&
                mouseX <= (this.sbx + this.a/2) + x_translation &&
                mouseY >= (this.sby - this.b/2) && 
                mouseY <= (this.sby + this.b/2) &&
                mPressed === true)
    }

    second_button_click_condition() {
        return ((mPressed === false && this.click === true &&
                (mouseX >= (this.sbx - this.a/2) + x_translation &&
                 mouseX <= (this.sbx + this.a/2) + x_translation &&
                 mouseY >= (this.sby - this.b/2) &&
                 mouseY <= (this.sby + this.b/2))) === true)
    }

    third_button_click_condition() {
        return ((mPressed === false) && (this.click === false) &&
                (mouseX < (this.sbx - this.a/2) + x_translation &&
                 mouseX > (this.sbx + this.a/2) + x_translation &&
                 mouseY < (this.sby - this.b/2) &&
                 mouseY > (this.sby + this.b/2)) === false)
    }

    slide() {

        if(this.first_button_click_condition()) {

            this.click = true;
            this.begin = false;

            this.sbx = mouseX - x_translation;

            if(mouseX >= windowWidth - this.a/2) {
                this.sbx = x_translation - this.a/2;
            } else if (mouseX <= this.a/2) {
                this.sbx = (-1) * x_translation + this.a/2;
            }

            this.trail();
            stroke(0);
            fill(150);     
            rectMode(CENTER);
            rect(this.sbx, this.sby, this.a, this.b, this.c);
            fill(255);     
            rect(this.sbx, this.sby, this.a-20, this.b-20, this.c);

            phi = asin(this.sbx/r);
            omega = 0;
            alpha = 0;

           } else if(this.second_button_click_condition()) {

                        this.trail();
                        stroke(0);
                        fill(255);     
                        rectMode(CENTER);
                        rect(this.sbx, this.sby, this.a, this.b, this.c);

                        this.begin = true;
                        this.click = false;

           } else if(this.third_button_click_condition())  {

                        this.begin = true;
                        this.click = false;

                        this.trail();
                        stroke(0);
                        fill(255);     
                        rectMode(CENTER);
                        rect(this.sbx, this.sby, this.a, this.b, this.c);
           } else {
                this.begin = true;
                this.click = false;
                this.trail();
                stroke(0);
                fill(255);     
                rectMode(CENTER);
                rect(this.sbx, this.sby, this.a, this.b, this.c);
           }

           if(this.begin === true && this.click === false) {
                alpha = -g*sin(phi);
                omega += alpha;
                phi += omega;
           }
    }

    trail() {
        stroke(0);
        line(-windowWidth, windowHeight*90/100, windowWidth, windowHeight*90/100);
        fill(0);
        rectMode(CENTER);
        rect(0, this.sby, windowWidth - this.a, this.a/8);
    }
}