class Background_Colour_Setter {
    
    constructor(y) {
        this.x = windowWidth/2; this.y = y;
        this.a = 400; this.b = this.a/4; this.c = this.a/8;
        this.d = 150;
        this.name = "Background Colour";
        this.click = false;

        this.rgb = [];
        this.rgb_initial_positioning = [];

        this.sliders_setup = function() {
            this.initial_x_red = map(red, 0, 255, this.d/2, width - this.d/2);
            this.initial_x_green = map(green, 0, 255, this.d/2, width - this.d/2);
            this.initial_x_blue = map(blue, 0, 255, this.d/2, width - this.d/2);
    
            this.rgb_initial_positioning[0] = this.initial_x_red;
            this.rgb_initial_positioning[1] = this.initial_x_green;
            this.rgb_initial_positioning[2] = this.initial_x_blue;
    
            this.initial_red = map(this.initial_x_red, this.d/2, width - this.d/2, 0, 255);
            this.initial_green = map(this.initial_x_green, this.d/2, width - this.d/2, 0, 255);
            this.initial_blue = map(this.initial_x_blue, this.d/2, width - this.d/2, 0, 255);
    
            this.rgb[0] = this.initial_red;
            this.rgb[1] = this.initial_green;
            this.rgb[2] = this.initial_blue;
        }

        this.sliders_setup();

        this.sliders = [];

        this.slider_red = new Slider(this.rgb_initial_positioning[0], (height*15/100), this.rgb[0], 0);
        this.slider_green = new Slider(this.rgb_initial_positioning[1], (height*30/100), this.rgb[1], 1);
        this.slider_blue = new Slider(this.rgb_initial_positioning[2], (height*45/100), this.rgb[2], 2);


        this.return_button = new Return_Button(50, 50);
        // this.save_button = new Save_Button(width/2, height*80/100);
        this.start_simulation_screen = new Simulation_Screen(windowWidth/2, windowHeight*90/100);
    }

    first_button_click_condition() {
        return (mouseX >= (this.x - this.a/2) && 
                mouseX <= (this.x + this.a/2) &&
                mouseY >= (this.y - this.b/2) && 
                mouseY <= (this.y + this.b/2) && 
                mPressed === true)
    }

    second_button_click_condition() {
        return ((this.click === true) && (mPressed === false) &&
                (mouseX >= (this.x - this.a/2) && 
                 mouseX <= (this.x + this.a/2) &&
                 mouseY >= (this.y - this.b/2) && 
                 mouseY <= (this.y + this.b/2)) === true)
    }

    third_button_click_condition() {
        return ((this.click === true) && (mPressed === false) &&
                (mouseX < (this.x - this.a/2) && 
                 mouseX > (this.x + this.a/2) &&
                 mouseY < (this.y - this.b/2) && 
                 mouseY > (this.y + this.b/2)) === false)
    }

    button() {
        stroke(0);
        fill(255);
        rectMode(CENTER);
        rect(this.x, this.y, this.a, this.b, this.c);
        fill(0);
        text(this.name, this.x, this.y);
        
        if(this.first_button_click_condition()) {
            stroke(0);
            fill(150);
            rectMode(CENTER);
            rect(this.x, this.y, this.a, this.b, this.c);     
            
            stroke(0);
            fill(255);
            rectMode(CENTER);
            rect(this.x, this.y, this.a-20, this.b-20, this.c);
        
            fill(0);
            text(this.name, this.x, this.y);

            this.click = true;
        } else if(this.second_button_click_condition()) {
            background_colour_selector = true;
            this.click = false;
        } else if(this.third_button_click_condition()) {
            this.click = false;
        }
    }

    sliders_draw() {

        this.slider_red.slide();
        this.slider_green.slide();
        this.slider_blue.slide();
        red = this.slider_red.colourp;
        green = this.slider_green.colourp;
        blue = this.slider_blue.colourp;
    }

    display() {
        debugger;
        background(red, green, blue);
        this.return_button.button();

        this.sliders_draw();

        text("red: " + red, width/2, height*22.5/100);
        text("green: " + green, width/2, height*37.5/100);
        text("blue: " + blue, width/2, height*52.5/100);

        // this.save_button.button();
        this.start_simulation_screen.button();
    }

    /* sliders_setup() {
        this.initial_x_red = map(red, 0, 255, this.d/2, width - this.d/2);
        this.initial_x_green = map(green, 0, 255, this.d/2, width - this.d/2);
        this.initial_x_blue = map(blue, 0, 255, this.d/2, width - this.d/2);

        this.rgb_initial_positioning[0] = this.initial_x_red;
        this.rgb_initial_positioning[1] = this.initial_x_green;
        this.rgb_initial_positioning[2] = this.initial_x_blue;

        this.initial_red = map(this.initial_x_red, this.d/2, width - this.d/2, 0, 255);
        this.initial_green = map(this.initial_x_green, this.d/2, width - this.d/2, 0, 255);
        this.initial_blue = map(this.initial_x_blue, this.d/2, width - this.d/2, 0, 255);

        this.rgb[0] = this.initial_red;
        this.rgb[1] = this.initial_green;
        this.rgb[2] = this.initial_blue;
    } */
}