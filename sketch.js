let x_translation;

let r, g;
let phi, omega, alpha;

let start, background_colour_selector, background_colour_saving;

let red, green, blue;
let red_saved, green_saved, blue_saved;

let initial_screen;

let mPressed = false;

function mousePressed() {
	mPressed = true;
}

function mouseReleased() {
	mPressed = false;
}


function setup() {
	createCanvas(windowWidth, windowHeight);
	smooth(2);

	red = 205; green = 205; blue = 205;
	red_saved = red; green_saved = green; blue_saved = blue;

	start = false;
	background_colour_saving = false;
	background_colour_selector = false;

	r = height*80/100;
	g = 0.01;
	phi = 0;
	omega = 0;
	alpha = 0;

	x_translation = windowWidth/2;

	// change font name;
	textSize(32);
	textAlign(CENTER, CENTER);

	initial_screen = new Initial_Screen();
}

function draw() {
	initial_screen.display();

	frameRate(144);
}
